content = []


def readContent():
    print("read input")
    with open("files/input", "r") as input:
        for line in input:
            content.append(int(line.rstrip()))


def calcDepth():
    print("calculating depth")
    counterDepthIncrease = 0
    for i in range(1, len(content)):
        if content[i - 1] < content[i]:
            counterDepthIncrease += 1
    return counterDepthIncrease


# Take in 3 Messurements, compare if those 3 are greater then the previous 3. Stop if no 3 Mess are avail.
def calcSlidingDepth():
    print("calculating depth with 3 messurements")
    counterDepthIncrease = 0
    for i in range(3, len(content)):
        if (content[i - 3] + content[i - 2] + content[i - 1]) < (content[i - 2] + content[i - 1] + content[i]):
            counterDepthIncrease += 1
    return counterDepthIncrease


if __name__ == "__main__":
    readContent()
    print(calcDepth())
    print(calcSlidingDepth())
