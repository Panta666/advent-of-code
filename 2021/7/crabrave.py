import statistics

import imath

crabs = {}


def readContent():
    print("read input")
    global crabs
    with open("files/input", "r") as input:
        crablist = list(map(int, input.read().rstrip().split(",")))
        crablist.sort()
        for crab in crablist:
            if crab in crabs.keys():
                crabs[crab] = crabs[crab] + 1
            else:
                crabs[crab] = 1


def calc_fuel_usage():
    median = statistics.median(crabs)
    fuel = 0
    for i in crabs:
        fuel += imath.abs(median - i)
    print(fuel)


def calc_new_fuel_usage():
    print(crabs)
    lowestFuel = 9999999999999999999

    for i in range(0, len(crabs.keys())):
        fuel = 0
        for j in list(crabs.keys()):
            if i != j:
                tempfuel = 1
                difference = (imath.abs(i - j))
                # print("Ausgang,Prüfung:",i,j)
                # print("Unterschied: ",difference)
                for f in range(1, (difference)):
                    tempfuel += f + 1
                # print("Tempfule: ",tempfuel)
                fuel += tempfuel * crabs.get(j)
        if fuel < lowestFuel:
            lowestFuel = fuel
    print(lowestFuel)

if __name__ == "__main__":
    print("Organize Crabs")
    readContent()
    # calc_fuel_usage()
    calc_new_fuel_usage()
