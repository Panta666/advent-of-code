reproduceLife = 0
bornLife = 8
resetLife = 6
animals = {}


def readContent():
    print("read input")
    global animals
    with open("files/input", "r") as input:
        animallist = list(map(int, input.read().rstrip().split(",")))
        for animal in animallist:
            if animal in animals.keys():
                animals[animal] = animals[animal] + 1
            else:
                animals[animal] = 1


def lifecycle(days):
    for day in range(0, days):
        for animalnumber in range(0, len(animals)):
            if animals[animalnumber] == 0:
                animals[animalnumber] = 6
                animals.append(8)
            else:
                animals[animalnumber] -= 1
    return len(animals)


def lifecycle2(days):
    global animals
    for day in range(0, days):
        tempAnimals = animals.copy()
        numberNewAnimals = 0
        # durchlaufe 8-1
        # wenn i in keys ist, nimm value von temp und setze auf animals i-1
        keylist = list(animals.keys())
        keylist.sort()
        for key in keylist:
            if key == reproduceLife:
                numberNewAnimals = animals[key]
            else:
                animals[key - 1] = tempAnimals[key]
            if key == keylist[-1]:
                animals[key] = 0
        animals[bornLife] = numberNewAnimals
        if resetLife in animals.keys():
            animals[resetLife] = animals[resetLife] + numberNewAnimals
        else:
            animals[resetLife] = numberNewAnimals


def countFishies():
    sum = 0
    for key in animals.keys():
        sum += animals[key]
    return sum


if __name__ == "__main__":
    print("Fishies")
    readContent()
    # print("Anzahl Tiere:", lifecycle(140))
    print(animals)
    lifecycle2(256)

    print(countFishies())
