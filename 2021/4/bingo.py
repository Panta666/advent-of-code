import numpy as np
numbers = []
gamefields = []


def readContent():
    print("read input")
    global numbers
    with open("files/input", "r") as input:
        numbers = list(map(int, input.readline().rstrip().split(",")))
        linearray = []
        for line in input:
            if line == "\n":
                if len(linearray) != 0:
                    gamefields.append(np.array(linearray).reshape(5, 10))
                linearray = []
            else:
                splitline = np.array(line.rstrip().split(), dtype=int)
                for number in splitline:
                    linearray.append(np.array((number, 0)))


def createArray():
    print("Mach sachen")
    for i in range(0, 5):
        line = ""
        for j in range(0, 10, 2):
            line += "," + str(gamefields[0][i, j])
        print(line)


def printRow(gamefiled, row):
    line = ""
    for colum in range(0, 10, 2):
        if line != "":
            line += " "
        line += str(gamefields[gamefiled][row, colum])
    print(line)


def printColum(gamefiled, colum):
    line = ""
    for row in range(0, 5):
        if line != "":
            line += "\n"
        line += str(gamefields[gamefiled][row, colum])
    print(line)


def findNumberInGamefield(gamefiled, number):
    for row in range(0, 5):
        for colum in range(0, 10, 2):
            if gamefields[gamefiled][row, colum] == number:
                return (row, colum)


def setFoundInGamefield(gamefield, row, colum):
    gamefields[gamefield][row, (colum + 1)] = 1


def sumUnmarkedNumbers(gamefield):
    sum = 0
    for row in range(0, 5):
        for colum in range(1, 10, 2):
            if gamefields[gamefield][row, colum] == 0:
                sum += gamefields[gamefield][row, colum - 1]
    return sum


def calcScroe(gamefield, number):
    return number * sumUnmarkedNumbers(gamefield)


def play():
    winningGamefields = []
    lastNumber = -1
    for number in numbers:

        for gamefield in range(0, len(gamefields)):
            if gamefield not in winningGamefields:
                finding = findNumberInGamefield(gamefield, number)
                if (finding != None):
                    setFoundInGamefield(gamefield, finding[0], finding[1])
                if gamefieldWins(gamefield):
                    print("Gamefiled wins")
                    print(gamefields[gamefield])
                    winningGamefields.append(gamefield)
                    lastNumber = number
    return calcScroe(winningGamefields.pop(), lastNumber)



def sumRow(gamefield, row):
    sum = 0
    for colum in range(1, 10, 2):
        sum += gamefields[gamefield][row, colum]
    return sum


def sumColum(gamefield, colum):
    sum = 0
    for row in range(0, 5):
        sum += gamefields[gamefield][row, colum + 1]
    return sum


def gamefieldWins(gamefield):
    for row in range(0, 5):
        for colum in range(0, 5):
            if sumRow(gamefield, row) == 5 or sumColum(gamefield, colum) == 5:
                return True
    return False


def printGamefileds():
    for gamefield in gamefields:
        print(gamefield)


if __name__ == "__main__":
    print("Bingo")
    readContent()
    print(play())
