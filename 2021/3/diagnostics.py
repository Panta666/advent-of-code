import numpy as np

content = []
lines = 0
bitmatrix = np.matrix


def readContent():
    print("read input")
    with open("files/input", "r") as input:
        for line in input:
            global lines
            lines += 1
            for char in line:
                if char != '\n':
                    content.append(int(char))


def generateMatrix():
    global bitmatrix
    bitmatrix = np.matrix(np.array(content).reshape(lines, 12))


def powerconsumption():
    print("Calculate powerconsumption")
    gammabits = ""
    epsilon = ""
    erg = np.sum(bitmatrix, axis=0)

    for bit in erg.reshape(-1, ).tolist()[0]:
        if bit > (lines / 2):
            gammabits += "1"
            epsilon += "0"
        else:
            gammabits += "0"
            epsilon += "1"
    return int(gammabits, 2) * int(epsilon, 2)


def getDecimalValueFromBitlist(bitlist):
    out = 0
    for bit in bitlist:
        out = (out << 1) | bit
    return out


def findMostCommonBit(newBitmatrix, mostCommon, index=0):
    # print("find most common bit in bitlist")
    if newBitmatrix.shape[0] == 1:
        return getDecimalValueFromBitlist(np.ravel(newBitmatrix).tolist())
    else:
        erg = np.sum(newBitmatrix, axis=0)
        if mostCommon == 1:
            condition = erg.reshape(-1, ).tolist()[0][index] >= (np.size(newBitmatrix, 0) / 2)
        else:
            condition = erg.reshape(-1, ).tolist()[0][index] < (np.size(newBitmatrix, 0) / 2)
        if condition:
            # print("mehr einsen")
            mostCommonBit = 1
        else:
            # print("mehr nullen")
            mostCommonBit = 0
        # ravel needed to flatten the filter
        newBitmatrix = newBitmatrix[np.ravel(newBitmatrix[:, index] == mostCommonBit)]
        return findMostCommonBit(newBitmatrix, mostCommon, index + 1)


# Most common bit should be 1
def getOxigenRating():
    print("get OxigenRating")
    return findMostCommonBit(bitmatrix, 1)


def getCO2ScrubberRating():
    print("get O2 Scrubber Rating")
    return findMostCommonBit(bitmatrix, 0)


def calcLifesupportRating():
    return getOxigenRating() * getCO2ScrubberRating()


if __name__ == "__main__":
    print("Diagnostics")
    readContent()
    generateMatrix()
    print(powerconsumption())
    print(calcLifesupportRating())
