import math

import numpy as np

field = np.zeros((1000, 1000), dtype=int)
fromToInstructions = []
allowedAngles = [45, -45, 135, -135]


def readContent():
    print("read input")

    with open("files/input", "r") as input:
        for line in input:
            fromToInstructions.append(line.rstrip().split(" -> "))


def getFromInstruciton(instruction):
    return list(map(int, instruction[0].split(",")))


def getToInstruction(instruction):
    return list(map(int, instruction[1].split(",")))


def getPositionsToSet(xyFrom, xyTo):
    xfrom = xyFrom[0]
    yfrom = xyFrom[1]
    xto = xyTo[0]
    yTo = xyTo[1]
    points = []
    angle = angleBetween((xfrom, yfrom), (xto, yTo))
    if xfrom == xto:
        if yfrom < yTo:
            # loop from yfrom -> yto
            for i in range(yfrom, (yTo + 1)):
                points.append([xto, i])
        else:
            # loop from yto -> yfrom
            for i in range(yTo, (yfrom + 1)):
                points.append([xfrom, i])
    elif yfrom == yTo:
        if xfrom < xto:
            # loop from xfrom -> xto
            for i in range(xfrom, (xto + 1)):
                points.append([i, yfrom])
        elif xfrom > xto:
            # loop from xto -> xfrom
            for i in range(xto, (xfrom + 1)):
                points.append([i, yfrom])
    elif angle in allowedAngles:
        if angle == 45:
            # rechts unten, y mehr, x mehr
            counter = 0
            for i in range(xfrom, (xto + 1)):
                points.append([i, yfrom + counter])
                counter += 1
        elif angle == 135:
            # rechts oben, y weniger, x mehr
            counter = 0
            for i in range(xfrom, (xto - 1), -1):
                points.append([i, (yfrom - counter)])
                counter -= 1
        elif angle == -45:
            # links unten, y mehr, x weniger
            counter = 0
            for i in range(yfrom, (yTo - 1), -1):
                points.append([(xfrom - counter), i])
                counter -= 1
        else:
            # links oben, y weniger, x weniger
            counter = 0
            for i in range(xfrom, (xto - 1), -1):
                points.append([i, (yfrom - counter)])
                counter += 1

    return points


def setPoints(points):
    for point in points:
        fieldvalue = field[point[1]][point[0]]
        field[point[1]][point[0]] = fieldvalue + 1


def createField():
    print("Create field")
    for instruction in fromToInstructions:
        xyFrom = getFromInstruciton(instruction)
        xyTo = getToInstruction(instruction)
        setPoints(getPositionsToSet(xyFrom, xyTo))


def calcOverlaps():
    overlaps = 0
    for x in range(0, field.shape[0]):
        for y in range(0, field.shape[1]):
            if field[x][y] > 1: overlaps += 1
    return overlaps


def angleBetween(p1, p2):
    angleRad = math.atan2(p2[1] - p1[1], p2[0] - p1[0])
    return math.degrees(angleRad)


if __name__ == "__main__":
    print("Hydtothermal Vents")
    readContent()
    createField()
    print(field)
    print("Overlaps: ", calcOverlaps())
