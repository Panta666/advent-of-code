content = []

"""
    first impression:
        forward X increases the horizontal position by X units.
        down X increases the depth by X units.
        up X decreases the depth by X units.
"""

"""
    Actual commands:    
        down X increases your aim by X units.
        up X decreases your aim by X units.
        forward X does two things:
            It increases your horizontal position by X units.
            It increases your depth by your aim multiplied by X.

"""


def readContent():
    print("read input")
    with open("files/input", "r") as input:
        for line in input:
            content.append(line.rstrip())


def driveSubmarine():
    x = 0
    depth = 0
    for command in content:
        splitCommand = command.split(" ")
        if splitCommand[0] == "forward":
            x += int(splitCommand[1])
        elif splitCommand[0] == "down":
            depth += int(splitCommand[1])
        elif splitCommand[0] == "up":
            depth -= int(splitCommand[1])
    return x * depth


def actualyDriveSubmarine():
    x = 0
    depth = 0
    aim = 0
    for command in content:
        splitCommand = command.split(" ")
        if splitCommand[0] == "forward":
            x += int(splitCommand[1])
            depth += aim * int(splitCommand[1])
        elif splitCommand[0] == "down":
            aim += int(splitCommand[1])
        elif splitCommand[0] == "up":
            aim -= int(splitCommand[1])
    return x * depth


if __name__ == "__main__":
    print("Submarine")
    readContent()
    print(actualyDriveSubmarine())
